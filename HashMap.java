package data_structure;// Java program to demonstrate implementation of our
// own hash table with chaining for collision detection

// linked list
class Node<K, V> {
    String key;
    Object value;

    // Reference to next node
    Node<K, V> next;

    // Constructor
    public Node(String key, Object value)
    {
        this.key = key;
        this.value = value;
    }
}

class Person {
    String firstName;
    String lastName;
    int age;

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "First Name : " + firstName + ", Last Name : " + lastName + ", Age : " + age;
    }
}

// Class to represent entire hash table
public class HashMap<K, V> {
    // bucketArray is used to store array of chains
    private ArrayList bucketArray;

    // Current capacity of array list
    private int numBuckets;

    // Current size of array list
    private int size;

    // Constructor (Initializes capacity, size and
    // empty chains.
    public HashMap()
    {
        bucketArray = new ArrayList();
        numBuckets = 10;
        size = 0;

        // Create empty chains
        for (int i = 0; i < numBuckets; i++)
            bucketArray.add(null);
    }

    public int size() { return size; }
    public boolean isEmpty() { return size() == 0; }

    // This implements hash function to find index
    // for a key
    private int getBucketIndex(String key)
    {
        int hashCode = hash_using_Djb2(key);
        int index = hashCode % numBuckets;
        index = index < 0 ? index * -1 : index;
        return index;
    }

    public int hash_using_Djb2(String str) {
        int hash = 0;
        for (int i = 0; i < str.length(); i++) {
            hash = str.charAt(i) + ((hash << 5) + hash);
        }
        return hash;
    }


    // Method to remove a given key
    public void remove(Person paramPerson) throws Exception
    {
        String key = paramPerson.getFirstName();
        // Apply hash function to find index for given key
        int bucketIndex = getBucketIndex(key);

        // Get head of chain
        Node<K, V> head = (Node<K, V>) bucketArray.get(bucketIndex);

        // Search for key in its chain
        Node<K, V> prev = null;
        while (head != null) {
            // If Key found
            if (head.key.equals(key)) {
                Person oldPerson = (Person) head.value;

                // if value matches!
                if (oldPerson.getFirstName().equals(paramPerson.getFirstName()) &&
                        oldPerson.getLastName().equals(paramPerson.getLastName()) &&
                        oldPerson.getAge() == paramPerson.getAge()) {

                    // Remove key
                    if (prev != null) //means not first element
                        prev.next = head.next;
                    else
                        bucketArray.set(bucketIndex, head.next);
                    break;
                }
            }

            // Else keep moving in chain
            prev = head;
            head = head.next;
        }

        // Reduce size
        size--;

    }

    // Method to update a given key
    public void updateNonKey(String type,
                             Person paramPerson,
                             Object newValue) throws Exception
    {
        String key = paramPerson.getFirstName();
        // Apply hash function to find index for given key
        int bucketIndex = getBucketIndex(key);

        // Get head of chain
        Node<K, V> head = (Node<K, V>) bucketArray.get(bucketIndex);

        // Search for key in its chain
        Node<K, V> prev = null;
        while (head != null) {
            // If Key found
            if (head.key.equals(key)) {
                Person oldPerson = (Person) head.value;

                // if value matches!
                if (oldPerson.getFirstName().equals(paramPerson.getFirstName()) &&
                    oldPerson.getLastName().equals(paramPerson.getLastName()) &&
                    oldPerson.getAge() == paramPerson.getAge()) {

                    if (type.equals("lastName")) {
                        paramPerson.setLastName((String) newValue);
                    }
                    else if  (type.equals("age")) {
                        paramPerson.setAge((int) newValue);
                    }

                    head.value = paramPerson;
                    break;
                }
            }

            // Else keep moving in chain
            prev = head;
            head = head.next;
        }
    }

    // Method to update a given key
    public void updateWithKey(String type,
                              Person paramPerson,
                              Object newValue) throws Exception
    {
        //remove old key
        this.remove(paramPerson);

        paramPerson.setFirstName((String) newValue);
        //create new key
        this.add(paramPerson.getFirstName(), paramPerson);

    }



    // Returns value for a key
    public Object[] get(String key) throws Exception
    {
        Object[] result = new Object[10];
        // Find head of chain for given key
        int bucketIndex = getBucketIndex(key);
        Node<K, V> head = ( Node<K, V>) bucketArray.get(bucketIndex);

        // Search key in chain
        int i = 0;
        while (head != null) {
            if (head.key.equals(key)) {
                result[i++] = head.value;
            }
            head = head.next;
        }

        // If key not found
        return result;
    }

    // Adds a key value pair to hash
    public void add(String key, Object value) throws  Exception
    {
        // Find head of chain for given key
        int bucketIndex = getBucketIndex(key);
        Node<K, V> head = (Node<K, V>) bucketArray.get(bucketIndex);

        // Check if key is already present
        boolean isCollision = false;
        while (head != null) {
            if (head.key.equals(key)) {
                //head.value = value;
                isCollision = true;
                break;
            }
            head = head.next;
        }

        // Insert key in chain
        size++;
        Node<K, V> newNode = new Node<K, V>(key, value);
        if (!isCollision) {
            head = (Node<K, V>) bucketArray.get(bucketIndex);
            newNode.next = head;
            bucketArray.set(bucketIndex, newNode);
        }
        else {
            head.next = newNode;
        }

        // If load factor goes beyond threshold, then
        // double hash table size to avoid collision
        if ((1.0 * size) / numBuckets >= 0.7) {
            ArrayList temp = bucketArray;
            bucketArray = new ArrayList();
            numBuckets = 2 * numBuckets;
            size = 0;
            for (int i = 0; i < numBuckets; i++)
                bucketArray.add(null);

            for (int i=0; i < temp.getSize(); i++) {
                Node<K, V> headNode = (Node<K, V>) temp.get(i);
                while (headNode != null) {
                    add(headNode.key, headNode.value);
                    headNode = headNode.next;
                }
            }
        }
    }

    private void addPerson(Person p1) throws  Exception{
        this.add(p1.firstName,  p1);
    }

    private void update(Person p, String param, Object value) throws  Exception {
        if (param == "lastName") {
            this.updateNonKey("lastName", p, (String) value);
        }
        else if  (param == "age") {
            this.updateNonKey("age", p, (int) value);
        }
        else if  (param == "firstName") {
            this.updateWithKey("firstName", p, (String) value);
        }

    }

    // Driver method to test Map class
    public static void main(String[] args) throws Exception
    {
        Person p1 = new Person("John", "Doe", 44);
        Person p2 = new Person("John", "Smith", 55);

        HashMap myMap = new HashMap();
        myMap.addPerson(p1);
        myMap.addPerson(p2);

        System.out.println("LIST OF PERSON: ");

        myMap.update(p2, "lastName", "Angelo");
        myMap.update(p1, "age", 70);

        myMap.update(p2, "firstName", "Michael");

        printMap(myMap, "John");
        printMap(myMap, "Michael");


    }

    static  void printMap(HashMap myMap, String key) throws Exception {
        Object[] result = myMap.get(key);
        if (result != null && result.length > 0) {
            for (int i=0; i < result.length; i++) {
                if (result[i] != null) {
                    System.out.println(result[i]);
                }
            }
        }
    }

}
